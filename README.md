# goodreads

A list of excellent books, games, tutorials, blogs, articles and videos.

## Resources

### Books

- [The C Programming Language](https://isbnsearch.org/isbn/9780131103627)
- [Modern Operating Systems](https://isbnsearch.org/isbn/9780133591620)
- [Parallel Computer Organization and Design](https://isbnsearch.org/isbn/9780521886758)
- [UNIX and Linux System Administration Handbook](https://isbnsearch.org/isbn/9780131480056)

### Games

- [overthewire](http://overthewire.org/wargames)
- [hackthebox](https://www.hackthebox.eu)
- [deadlockempire](https://deadlockempire.github.io)
- [nandgame](http://nandgame.com)

### Tutorials

- [eudyptula](http://eudyptula-challenge.org)
- [exercism](https://exercism.io)
- [arjunsreedharan](https://arjunsreedharan.org/post/82710718100/kernels-101-lets-write-a-kernel)
- [kernelnewbies](https://kernelnewbies.org/KernelBuild)
- [os-tutorial](https://github.com/cfenollosa/os-tutorial)

### Blogs

- [drewdevault](https://drewdevault.com)
- [maizure](http://www.maizure.org/projects)
- [david942j](https://david942j.blogspot.com)
- [burntsushi](https://blog.burntsushi.net/about)


### Articles

- https://lwn.net/Articles/767630
- https://www.gribblelab.org/CBootCamp/7_Memory_Stack_vs_Heap.html
- http://boxbase.org/entries/2019/feb/25/riscv-asm-can-be-fun/

### Videos

- https://www.youtube.com/watch?v=ZMZpH4yT7M0
- https://www.youtube.com/watch?v=bM6N-vgPlyQ
- https://www.youtube.com/watch?v=o_AIw9bGogo
- https://www.youtube.com/watch?v=ZMZpH4yT7M0
- https://www.youtube.com/watch?v=KP_bKvXkoC4
- https://www.youtube.com/watch?v=xm5AlNj-I5M
- https://www.youtube.com/watch?v=8T3VxGrrJwc
- https://www.youtube.com/watch?v=wN6IwNriwHc